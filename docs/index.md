---
author: Ronan Charpentier
title: 🏡 Accueil
---


😊  Bienvenue !

On veut une fonction `Euclide` qui renvoie le PGCD de deux entiers par la méthode d'Euclide.


??? note "Rappel"

  On divise le plus grand des deux nombres par le plus petit et on recommence avec le diviseur et le reste jusqu'à ce que le reste soit nul. Dans une DE $a=bq+r$ le quotient de $a$ par $b$ est donné par `#!py a//b` et le reste par `#!py a%b`. 

??? example "Exemples"

    ```pycon title=""
    >>> Euclide(22,12)
    2
    >>> Euclide(22,7)
    1
    ```

=== "Version vide"
    {{ IDE('euclide_vide')}}






