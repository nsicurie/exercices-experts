# --- PYODIDE:code --- #
def Euclide(a,b):
    ...


# --- PYODIDE:corr --- #
def Euclide(a,b):
    while b!=0:
        a,b=b,a%b
    return a

# --- PYODIDE:tests ---#
assert Euclide(22,12)==2
assert Euclide(22,7)==1

# ----PYODIDE:secrets---#
from random import randrange

for i in range(10):
    a=randrange(2,200)
    b=randrange(1,300)
    res=Euclide(a,b)
    u,v=a,b
    while v!=0:
        u,v=v,u%v
    assert res==u, f"Erreur avec a={a} et b={b}"

